import configparser
from os import listdir
from functions import *
from mappers import Mappers

if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read('../config.ini')

    file_names = [config['Settings']['Path'] + '/Data/' + f for f in
                  listdir(config['Settings']['Path'] + '/Data/')]
    mapper = Mappers()
    city_to_file = dict(zip(mapper.city_names, file_names))

    res = print_info(city_to_file, mapper.weight_to_col, mapper.volume_to_col)

