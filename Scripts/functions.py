import re
import pandas as pd
from functools import reduce


def remove_chars(city):
    chars_to_remove = '()-'
    for character in chars_to_remove:
        city = city.replace(character, ' ')
    return city


def process_city(city):
    city = ' '.join(remove_chars(city).split()).lower()
    if city == 'санкт петербург':
        return 'спб'
    return city


def get_city(name):
    if name == 'Откуда':
        city = input('Введите пункт отправления: ')
    else:
        city = input('Введите пункт назначения: ')
    print(f'{name}: {city}')
    return process_city(city)


def get_numbers(name):
    vals = [float(elem) for elem in re.split(r'[^\d.-]', input(f'Введите {name.lower()}: ')) if elem]
    if any([val < 0 for val in vals]):
        print(f'{name} не может быть отрицательным')
        return -1
    elif len(vals) == 3 and name == 'Объем':
        print(f'Длина: {vals[0]} м, ширина: {vals[1]} м, высота: {vals[2]} м.')
        res = reduce((lambda x, y: x * y), vals)
        print(f'{name}: {res:.4} м3.')
        return res
    elif len(vals) == 1:
        if name == 'Вес':
            print(f'{name}: {vals[0]} кг.')
        else:
            print(f'{name}: {vals[0]} м3.')
        return vals[0]
    else:
        print('Недопустимое количество цифр')
        return -1


def select_column(mapping_dict, val, col_names):
    if val > max(mapping_dict.keys()):
        return col_names[mapping_dict[max(mapping_dict.keys())]]
    return col_names[mapping_dict[next(x for x in mapping_dict.keys() if x >= val)]]


def print_info(city_to_file, weight_to_col, volume_to_col):
    while True:
        city_from = get_city('Откуда')
        if city_from in city_to_file.keys():
            break
        else:
            print('Пункт отправления не найден')
    df = pd.read_csv(city_to_file[city_from])
    df['Город'] = df['Город'].apply(lambda x: process_city(x))
    while True:
        city_to = get_city('Куда')
        if city_to in df['Город'].to_list():
            break
        else:
            print('Пункт назначения не найден')

    min_price = df.loc[df['Город'] == city_to, 'Минимальная стоимость'].values[0]
    print(
        f'Минимальная стоимость посылки из пункта \"{city_from.capitalize()}\" в пункт \"{city_to.capitalize()}\" составляет {min_price} руб.')

    while True:
        weight = get_numbers('Вес')
        if weight != -1:
            break
    while True:
        volume = get_numbers('Объем')
        if volume != -1:
            break

    w_k = df.loc[df['Город'] == city_to, select_column(weight_to_col, weight, df.columns)].values[0]
    print(w_k)
    v_k = df.loc[df['Город'] == city_to, select_column(volume_to_col, volume, df.columns)].values[0]
    print(v_k)

    total_min_price = int(max(min_price, w_k * weight, v_k * volume))
    print(f'Минимальная стоимость отправления равна {total_min_price} руб.')
    return total_min_price
