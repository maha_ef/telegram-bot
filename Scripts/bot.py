from telegram.ext.updater import Updater
from telegram.update import Update
from telegram.ext.callbackcontext import CallbackContext
from telegram.ext.commandhandler import CommandHandler
from telegram.ext.messagehandler import MessageHandler
from telegram.ext.filters import Filters
import functions
import configparser


def start(update: Update, context: CallbackContext):
    update.message.reply_text(
        "Я бот компании Mega Trans. \nНапишите /help для того, чтобы увидеть все доступные команды.")


def help(update: Update, context: CallbackContext):
    help_text = ({
        'url': '/url - Сайт компании Mega Trans',
        'number': '/number - Номера компании Mega Trans',
        'mail': '/mail - Почта для обратной связи',
        'calc': '/calc - Рассчитать стоимость отправления'
    })
    update.message \
        .reply_text('\tДоступные команды: \n\n ' + help_text['url'] + '\n\n' +
                    help_text['number'] + '\n\n' + help_text['mail'] + '\n\n' +
                    help_text['calc'])


def url(update: Update, context: CallbackContext):
    update.message.reply_text(f'Официальный сайт компании: \n{config["Contacts"]["URL"]}')


def number(update: Update, context: CallbackContext):
    update.message.reply_text('Официальные номера компании: \n' + config["Contacts"]["Number1"] +
                              '\n' + config["Contacts"]["Number2"])


def mail(update: Update, context: CallbackContext):
    update.message.reply_text(f'Электронная почта: \n{config["Contacts"]["Mail"]}')


def calc(update: Update, context: CallbackContext):
    while True:
    print(update.message.chat_id)
    print(update.message.text)
    update.message.reply_text('Тут я пока не успел)')


def echo(update: Update, context: CallbackContext):
    return update.message.text


# def echo(update, context):
#     while True:
#         updater.dispatcher.add_handler(MessageHandler(Filters.text, echo))
#
#         city_from = functions.get_city('Откуда')
#         if city_from in functions.city_to_file.keys():
#             break
#         else:
#             print('Пункт отправления не найден')
#
#     update.message.reply_text(update.message.text)


if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read('../config.ini')
    updater = Updater(config['Telegram']['API_Token'],
                      use_context=True)

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CommandHandler('help', help))
    updater.dispatcher.add_handler(CommandHandler('url', url))
    updater.dispatcher.add_handler(CommandHandler('number', number))
    updater.dispatcher.add_handler(CommandHandler('mail', mail))
    updater.dispatcher.add_handler(CommandHandler('calc', calc))
    updater.dispatcher.add_handler(MessageHandler(Filters.text, echo))

    updater.start_polling()
